package co.datainsider.service

import com.twitter.util.Future
import co.datainsider.domain.request.LinkShortenerInfo

trait LinkShortenerService{
  def create(linkShortenerInfo :LinkShortenerInfo):  Future[LinkShortenerInfo];
  def delete(linkShortenerInfo :LinkShortenerInfo): Future[Boolean];
  def update(linkShortenerInfo :LinkShortenerInfo): Future[Boolean];
  def get(shortenLink: String): Future[LinkShortenerInfo];
  def list() : Future[Seq[LinkShortenerInfo]]
}

class LinkShortenerServiceImpl extends LinkShortenerService {
  override def create(linkShortenerInfo: LinkShortenerInfo): Future[LinkShortenerInfo] = ???

  override def delete(linkShortenerInfo: LinkShortenerInfo): Future[Boolean] = ???

  override def get(shortenLink: String): Future[LinkShortenerInfo] = ???

  override def list(): Future[Seq[LinkShortenerInfo]] = ???

  override def update(linkShortenerInfo: LinkShortenerInfo): Future[Boolean] = ???
}
