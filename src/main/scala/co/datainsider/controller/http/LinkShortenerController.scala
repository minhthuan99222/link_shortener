package co.datainsider.controller.http

import com.twitter.finagle.http.Request

import javax.inject.Singleton
import com.twitter.finatra.http.Controller

@Singleton
class LinkShortenerController extends Controller {
    post("/link_shortener"){ request: Request =>
      logger.info(s"create short link ${request.path}")
    }
}
