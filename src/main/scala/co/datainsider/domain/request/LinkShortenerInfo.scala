package co.datainsider.domain.request

case class LinkShortenerInfo(destinationLink: String, shortenLink: String, title: String)
