package co.datainsider.domain.request

import co.datainsider.domain.{UserID, UserInfo}

case class PutCacheRequest(userID: UserID, userInfo: UserInfo) {}
