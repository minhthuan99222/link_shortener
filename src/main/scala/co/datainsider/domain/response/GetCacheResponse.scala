package co.datainsider.domain.response

import co.datainsider.domain.UserInfo

/**
 * Created by SangDang on 9/16/16.
 */
case class GetCacheResponse(info: UserInfo)
